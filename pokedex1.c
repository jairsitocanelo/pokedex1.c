#include <studio.h>
#include <stdLib.h>
#include <string.h>

struct Pokédex {
    char name[100];
    int id;
    char type[50];
    int level;
};
// This function is used to store each trainers's data such as ID, name, age, hometown. 
struct trainers {
    char name[100];
    int age;
    int experience;
    int pokeballs;
    struct Pokédex team[6]; // an array of 6 Pokédex structs
};

// Uses array of characters to store names of trainers and uses a for loop to print out names
int main() {
  char *trainers[] = { "Ash Ketchum", "Gary Oak", "Brock", "Misty", "Lt. Surge", "Erika", "Koga", "Sabrina", "Blaine", "Giovanni" };
  int num_trainers = sizeof(trainers) / sizeof(trainers[0]);
  
  printf("POKEDEX - TRAINERS\n");
  printf("------------------\n");
  
  for (int i = 0; i < num_trainers; i++) {
    printf("%d. %s\n", i + 1, trainers[i]);
  }
  
  return 0;
}

 trainers = [ 
    {
        "name": "Ash Ketchum",
        "age": 10,
        "hometown": "Pallet Town",
        "pokemon": [1, 2, 3]
    },
    {
        "name": "Gary Oak",
        "age": 10,
        "hometown": "Pallet Town",
        "pokemon": [1, 2]
    },
    {
        "name": "Misty",
        "age": 11,
        "hometown": "Cerulean City",
        "pokemon": [1, 2]
    }
]

// the function is an example of how you need to input a number in order to find a specific pokemon
pokedex = {
    1: {
        "name": "Bulbasaur",
        "type": "Grass",
        "hp": 45,
        "attack": 49,
        "defense": 49,
        "special_attack": 65,
        "special_defense": 65,
        "speed": 45
    },
    2: {
        "name": "Ivysaur",
        "type": "Grass",
        "hp": 60,
        "attack": 62,
        "defense": 63,
        "special_attack": 80,
        "special_defense": 80,
        "speed": 60
    },
}

struct PokédexNode {
    struct Pokédex data;
    struct PokédexNode* next;
};
//function to add new pokemon to list 
struct PokédexNode* createNewPokemonNode(struct Pokédex newPokemon) {
    struct PokédexNode* newNode = (struct PokédexNode*) malloc(sizeof(struct PokédexNode));
    newNode->data = newPokemon;
    newNode->next = NULL;
    return newNode;
}
struct Pokédex myPokemon = {"Bulbasaur", 25, "Grass", 5};
struct PokédexNode* newNode = createNewPokemonNode(myPokemon);

//like the function above used to create a new node and allocate memory 
struct trainerNode {
    struct trainers data;
    struct trainerNode* next;
};
//function to add new trainers to list 
struct trainerNode* createNewTrainerNode(struct trainers newTrainer) {
    struct trainerNode* newNode = (struct trainerNode*) malloc(sizeof(struct trainerNode));
    newNode->data = newTrainer;
    newNode->next = NULL;
    return newNode;
}
struct trainers myTrainer = {"Ash Ketchum", 10, 5, 10};
struct trainerNode* newNode = createNewTrainerNode(myTrainer);

void AddPlayerToList(struct PlayerList *list, char name[50], int score) {
  if (list->count < 100) {
    strcpy(list->players[list->count].name, name);
    list->players[list->count].score = score;
    list->count++;
  }
}

struct PlayerNode {
    char name[20];
    int level;
    // fields required to work together to function corretly 
};

//this function is used to search the trainer list for a name 
PlayerNode* FindPlayer(struct PlayerNode pokedex[], char name[]) {
    for (int i = 0; i < sizeof(pokedex)/sizeof(pokedex[0]); i++) {
        if (strcmp(pokedex[i].name, name) == 0) {
            return &pokedex[i];
        }
    }
    return NULL;
}







